//
//  SendHubTestTests.m
//  SendHubTestTests
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SHCoreDataManager.h"

@interface SendHubTestTests : XCTestCase

@end

@implementation SendHubTestTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

//Core Data tests

-(void)testManagedObjectContextExists
{
    NSManagedObjectContext *context = [[SHCoreDataManager sharedManager]managedObjectContext];
    XCTAssertTrue(context != nil, @"Check if %@ is nil",context);
}

//
@end
