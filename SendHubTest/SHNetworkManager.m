//
//  SHNetworkManager.m
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//
//  This class manages specific HTTP Request functions. We use blocks here because the app is so simple
//  If the app became increasingly complex with many network requests and had a need for more intense
//  error handling/etc we might implement a network helper class that receives delegate messages from
//  NURLRequests/Connections. This would also reduce some code duplication as you see below - having
//  multiple blocks each managing their own errors

#import "SHNetworkManager.h"
#import "ConnectionManager.h"
#import "SHAlertManager.h"
#import "SHCoreDataManager.h"

@implementation SHNetworkManager

+(id)sharedManager
{
    static SHNetworkManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc]init];
    });
    return sharedManager;
}

-(id)init
{
    if (self = [super init])
    {
              
    }
    return self;
}

-(BOOL)canDetectInternetAndHost
{
    if (![[ConnectionManager sharedManager]internetActive])
    {
        [SHAlertManager showErrorAlertWithTitle:ERROR_TITLE_CONNECTION errorDescription:ERROR_DESCRIPTION_INTERNET_CONNECTION];
        return NO;
    }
    else if ([[ConnectionManager sharedManager]hostActive])
    {
        [SHAlertManager showErrorAlertWithTitle:ERROR_TITLE_CONNECTION errorDescription:ERROR_DESCRIPTION_HOST_CONNECTION];
        return NO;
    }
    
    return YES;
}

-(void)getContactsFromServer
{
    if (![self canDetectInternetAndHost])
        return;
    
    //URL NOTES FROM DOCUMENTATION
    //To view all your contacts, make a GET request to: https://api.sendhub.com/v1/contacts/?username=NUMBER&api_key=APIKEY
  
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@%@",URL_CONTACTS_PART1,ACCOUNT_PHONE,URL_PART2,API_KEY];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    NSLog(@"Requesting Contacts From Server");
    
    //It is very important to asynchronously download data so the main thread is not blocked (thus blocking user interaction)
    //while also making sure that we end up back on the main thread after the request completes so UI can update/function properly
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [SHAlertManager showErrorAlertWithTitle:ERROR_TITLE_MESSAGE_GENERIC errorDescription:[error localizedDescription]];
         }
         else
         {
             [[SHCoreDataManager sharedManager] parseAndSaveContactsFromJSON:data error:&error];
         }
         
     }];
    
}

-(void)sendMessage:(NSString*)message contactID:(NSNumber*)contactID
{
    if (![self canDetectInternetAndHost])
        return;
    
    NSArray *contacts = [NSArray arrayWithObjects:contactID, nil];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:message,JSON_MESSAGE_TEXT,contacts,JSON_MESSAGE_RECIPIENTS, nil];
    NSError *error = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@%@",URL_SEND_MESSAGE_PART1,ACCOUNT_PHONE,URL_PART2,API_KEY];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];

    request.HTTPMethod = HTTP_POST;
    [request setValue:HTTP_HEADER forHTTPHeaderField:HTTP_HEADERFIELD];
    request.HTTPBody = jsonData;
    request.timeoutInterval = 20.0;
    NSLog(@"JSON: %@,",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
         {
             [SHAlertManager showErrorAlertWithTitle:ERROR_TITLE_MESSAGE_GENERIC errorDescription:[error localizedDescription]];
         }
         else
         {
            
             NSError *localError = nil;
             NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
             NSLog(@"ResponseData: %@",[parsedObject description]);
         }
         
     }];
    
    
}




@end
