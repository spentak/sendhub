//
//  SHDetailViewController.m
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import "SHDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SHFormValidation.h"
#import "SHAlertManager.h"
#import "SHNetworkManager.h"

@interface SHDetailViewController ()
- (void)configureView;
@end

@implementation SHDetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.textFieldContactName.text = [[self.detailItem valueForKey:CD_ATTRIBUTE_CONTACT_NAME] description];
        self.textFieldContatctPhone.text = [[self.detailItem valueForKey:CD_ATTRIBUTE_CONTACT_PHONE] description];
    }
    
    [self setSendButtonEnabledState];
}

-(void)setSendButtonEnabledState
{
    if ([self.textFieldContatctPhone.text length] < VALID_MINIMUM_PHONE_LENGTH)
        [self.sendButton setEnabled:NO];
    else
        [self.sendButton setEnabled:YES];
}

-(void)initViews
{
    //Give the UITextView a border so the user can know to tap in it
    self.textViewMessage.layer.masksToBounds=YES;
    self.textViewMessage.layer.borderColor=[[UIColor grayColor]CGColor];
    self.textViewMessage.layer.borderWidth= .50f;
    [self.textFieldContatctPhone setDelegate:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initViews];
    [self configureView];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIAlertView*)showValidationError:(NSString*)title errDescription:(NSString*)description
{
    return [SHAlertManager showErrorAlertWithTitle:title  errorDescription:description];
}

//This method needs some refactoring - needs to be simplified - remove some duplication
-(BOOL)allFieldsValid
{
    //We use magic numbers for some of the arguments here for sake of simplicity
    //in a full-scale application we might a class that handles various validation lengths
    //for text messages and phone numbers, etc (this would be especially needed if the app is used in differen
    //countries with different validation critera)
    
    //Validate text message field
    if (![SHFormValidation fieldHasText:self.textViewMessage.text minimumCharLength:1 maxCharLength:5000])
    {
        [[self showValidationError:VALID_ERROR_TITLE errDescription:VALID_NO_TEXT_MESSAGE]show];
        return NO;
    }
    
    //Please note that changing the phone number doesn't really do anything simply because we are
    //using contact ID's to send messages. If this was a finished application, when you updated the
    //phone number it would save locally as well as post the changes to the serverd
    if (![SHFormValidation fieldHasText:self.textFieldContatctPhone.text minimumCharLength:VALID_MINIMUM_PHONE_LENGTH maxCharLength:12])
    {
        NSLog(@"Phone Length: %i",[self.textFieldContatctPhone.text length]);
        [[self showValidationError:VALID_ERROR_TITLE errDescription: VALID_PHONE_LENGTH_MESSAGE]show];
        return NO;
    }
    
    if (![SHFormValidation fieldHasText:self.textFieldContactName.text minimumCharLength:1 maxCharLength:100])
    {
        [[self showValidationError:VALID_ERROR_TITLE errDescription: VALID_CONTACT_NAME_MESSAGE]show];
        return NO;
    }
    
    return YES;
}

-(IBAction)processMessage:(id)sender
{
    if ([self allFieldsValid])
    {
        [[SHNetworkManager sharedManager]sendMessage:self.textViewMessage.text contactID:[self.detailItem valueForKey:CD_ATTRIBUTE_CONTACT_ID]];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self setSendButtonEnabledState];
    return YES;
}


@end
