//
//  SHCoreDataManager.h
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SHAppDelegate.h"

@interface SHCoreDataManager : NSObject
{
    SHAppDelegate *appDelegate;
}
@property (readonly,strong,nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly,strong,nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly,strong,nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

-(void)saveContext;
-(void)parseAndSaveContactsFromJSON:(NSData*)objectNotation error:(NSError**)error;
-(NSURL *)applicationDocumentsDirectory;
-(NSArray*)getContactList;
-(NSManagedObjectContext *)managedObjectContext;
-(BOOL)doContactsExistInCoreData;
+ (id)sharedManager;

@end
