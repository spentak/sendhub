//
//  SHAppDelegate.m
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import "SHAppDelegate.h"
#import "SHCoreDataManager.h"
#import "SHContactListViewController.h"
#import "SHNetworkManager.h"

@implementation SHAppDelegate



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /* 
        If there are no records in Core Data we download them from the Internet
        In a full scale application we would first check and see if there are records (like we do now)
        if records existed we would load them. Then we might immediately check the server and see if the records on the server
        are different from our local recordset(would need some type of version id from the record set on the server which would be saved locally,
        if the server recordset id and the local id didn't match it would mean the server contacts had been updated and we would download accordingly.
    */
    
    if ([[SHCoreDataManager sharedManager]doContactsExistInCoreData] == NO)
    {
        //In a full-scale application we wouldn't use a hard coded phone number - obviously this would be retrieved from somewhere (like user input)
        //For sake of the demo we use a hard coded phone number
        [[SHNetworkManager sharedManager]getContactsFromServer];
    }
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [[SHCoreDataManager sharedManager]saveContext];
}

@end
