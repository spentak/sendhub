//
//  SHConstants.h
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//


//I thought it would be useful to keep constants all in one file
//the other option is using extern on these and putting them in the classes
//that they relate too, but that takes more work and seems to serve the same purpose

#define API_KEY @"23126cc08e0aea9ac6540f99e055068d51733139"
#define ACCOUNT_PHONE @"5592807628"

//URLS
#define URL_CONTACTS_PART1 @"https://api.sendhub.com/v1/contacts/?username="
#define URL_SEND_MESSAGE_PART1 @"https://api.sendhub.com/v1/messages/?username="

#define URL_PART2 @"&api_key="

#define URL_HOST @"https://api.sendhub.com"

//Web status codes
#define STATUS_OK 200
#define STATUS_MESSAGE_CREATED 201

//Note that in a full-scale application that error messages and validation errors
//would be entered in a Localizable.strings file so they could be localized into different languages
//Error & validation messages and values
#define ERROR_TITLE_MESSAGE_GENERIC @"Error"
#define VALID_NO_TEXT_MESSAGE @"Please enter a message before sending"
#define VALID_ERROR_TITLE @"Input Error"
#define VALID_MINIMUM_PHONE_LENGTH 9 //Made this 9 because the final keystroke doesn't get called in the delegate method - need to refactor this 
#define VALID_PHONE_LENGTH_MESSAGE @"Please enter a valid phone number"
#define VALID_CONTACT_NAME_MESSAGE @"Please enter a valid name"


#define ERROR_TITLE_CONNECTION @"Connection Error"
#define ERROR_DESCRIPTION_INTERNET_CONNECTION @"Cannot connect to Internet. Please check your Wifi or Cellular settings"
#define ERROR_DESCRIPTION_HOST_CONNECTION @"Cannot reach the host server. Please try again later."

//JSON Key-values
#define JSON_CONTACTS @"objects"
#define JSON_CONTACT_NAME @"name"
#define JSON_CONTACT_ID @"id"
#define JSON_CONTACT_PHONE_NUMBER @"number"
#define JSON_MESSAGE_RECIPIENTS @"contacts"
#define JSON_MESSAGE_TEXT @"text"

//Core Data Key-values
#define CD_CACHE @"SendHubCache"
#define CD_ENTITY_CONTACT @"Contact"
#define CD_ATTRIBUTE_CONTACT_NAME @"name"
#define CD_ATTRIBUTE_CONTACT_PHONE @"phoneNumber"
#define CD_ATTRIBUTE_CONTACT_ID @"contactID"


//Notification Center keys
#define NOTIF_NEW_CONTACTS_AVAILABLE @"newcontactsavailable"

//HTTP
#define HTTP_POST @"POST"
#define HTTP_HEADER @"application/json"
#define HTTP_HEADERFIELD @"Content-Type"

