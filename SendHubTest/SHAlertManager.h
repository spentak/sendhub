//
//  SHAlertManager.h
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHAlertManager : NSObject

+(UIAlertView*)showErrorAlertWithTitle:(NSString*)title errorDescription:(NSString*)description;

@end
