//
//  SHMasterViewController.h
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

@interface SHMasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;


@end
