//
//  ConnectionManager.h
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Reachability;

@interface ConnectionManager : NSObject {
    Reachability *internetReachable;
    Reachability *hostReachable;
}

@property BOOL internetActive;
@property BOOL hostActive;

- (void) checkNetworkStatus:(NSNotification *)notice;
+ (id)sharedManager;
@end