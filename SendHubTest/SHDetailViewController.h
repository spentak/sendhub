//
//  SHDetailViewController.h
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SHDetailViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UITextField *textFieldContactName;
@property (weak, nonatomic) IBOutlet UITextField *textFieldContatctPhone;
@property (weak, nonatomic) IBOutlet UITextView *textViewMessage;
@property (weak,nonatomic) IBOutlet UIButton *sendButton;
-(IBAction)processMessage:(id)sender;
@end
