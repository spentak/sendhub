//
//  SHFormValidation.m
//  SendHubTest
//
//  Created by Mark Price on 10/15/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//
//  This is some really *cheap* validation. There are more validation methods
//  we could use to make validation more effective

#import "SHFormValidation.h"

@implementation SHFormValidation

+(BOOL)fieldHasText:(NSString*)text minimumCharLength:(NSInteger)minCharLength maxCharLength:(NSInteger)maxCarLength
{
    if (text == nil)
        return NO;
    else if ([text length] >= minCharLength && [text length] <= maxCarLength)
        return YES;
    else return NO;
}

@end
