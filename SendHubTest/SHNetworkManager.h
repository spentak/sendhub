//
//  SHNetworkManager.h
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SHAppDelegate.h"


@interface SHNetworkManager : NSObject
{
    NSMutableData *responseData;

}

-(void)getContactsFromServer;
-(void)sendMessage:(NSString*)message contactID:(NSNumber*)contactID;
+ (id)sharedManager;
@end
