//
//  SHCoreDataManager.m
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import "SHCoreDataManager.h"
#import "Contact.h"

@implementation SHCoreDataManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+(id)sharedManager
{
    static SHCoreDataManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc]init];
    });
    return sharedManager;
}

-(id)init
{
    if (self = [super init])
    {
        appDelegate = [UIApplication sharedApplication].delegate;
    }
    return self;
}

//I would like to refactor this so it performs one distinct function rather than two
-(void)parseAndSaveContactsFromJSON:(NSData*)objectNotation error:(NSError**)error
{
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:objectNotation options:0 error:&localError];
    
    if (localError != nil)
    {
        *error = localError;
        NSLog(@"Error Parsing Contacts: %@",[localError localizedDescription]);
        
    }
    
    NSArray *contacts = [parsedObject valueForKey:JSON_CONTACTS];
    
    //NSLog(@"Contact Parse Data: %@",[parsedObject description]);

    for (NSDictionary *dict in contacts)
    {
        Contact *contact = [NSEntityDescription insertNewObjectForEntityForName:CD_ENTITY_CONTACT inManagedObjectContext:[self managedObjectContext]];
        
        for (id key in [dict allKeys])
        {
            if ([key isEqual: JSON_CONTACT_NAME])
                contact.name = [dict valueForKey:key];
            else if ([key isEqual:JSON_CONTACT_ID])
                contact.contactID = [dict valueForKey:key];
            else if ([key isEqual:JSON_CONTACT_PHONE_NUMBER])
                contact.phoneNumber = [dict valueForKey:key];
        }
        
        NSLog(@"Parsed Contact Info - Name: %@, ID: %@, PHONE: %@",contact.name,[contact.contactID stringValue],contact.phoneNumber);
    }
    
    [self saveContext];
    
}

//To grab the contacts manually if needed
-(NSArray*)getContactList
{
    NSArray *contactList = nil;
    
    NSEntityDescription *contactEntity = [NSEntityDescription entityForName:CD_ENTITY_CONTACT inManagedObjectContext:[self managedObjectContext]];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:contactEntity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]initWithKey:CD_ATTRIBUTE_CONTACT_NAME ascending:YES];
    [request setSortDescriptors:@[sortDescriptor]];
    
    NSError *error = nil;
    contactList = [[self managedObjectContext]executeFetchRequest:request error:&error];
    
    if (error)
    {
        NSLog(@"Contacts Core Data Fetch: %@",[error localizedDescription]);
    }
    
    return contactList;
}

-(BOOL)doContactsExistInCoreData
{
    NSEntityDescription *contactEntity = [NSEntityDescription entityForName:CD_ENTITY_CONTACT inManagedObjectContext:[self managedObjectContext]];
    NSFetchRequest *request = [[NSFetchRequest alloc]init];
    [request setEntity:contactEntity];
    
    NSError *error = nil;
    NSUInteger count = [[self managedObjectContext]countForFetchRequest:request error:&error];
    
    if (count == NSNotFound || count == 0)
        return NO;
    else
        return YES;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
#warning add error handling here
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"SendHubTest" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"SendHub.sqlite"];
    
#warning implement error handling here
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


@end

