//
//  SHAlertManager.m
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import "SHAlertManager.h"

@implementation SHAlertManager


-(id)init
{
    if (self = [super init])
    {
        
        
    }
    return self;
}

+(UIAlertView*)showErrorAlertWithTitle:(NSString*)title errorDescription:(NSString*)description
{
    return [[UIAlertView alloc]initWithTitle:title message:description delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
}


@end
