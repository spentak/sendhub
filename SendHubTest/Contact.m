//
//  Contact.m
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import "Contact.h"


@implementation Contact

@dynamic name;
@dynamic phoneNumber;
@dynamic contactID;

@end
