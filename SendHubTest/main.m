//
//  main.m
//  SendHubTest
//
//  Created by Mark Price on 10/14/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SHAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SHAppDelegate class]));
    }
}
