//
//  SHFormValidation.h
//  SendHubTest
//
//  Created by Mark Price on 10/15/13.
//  Copyright (c) 2013 Mark Price. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SHFormValidation : NSObject
+(BOOL)fieldHasText:(NSString*)text minimumCharLength:(NSInteger)minCharLength maxCharLength:(NSInteger)maxCarLength;
@end
